package at.paxfu.mct.listeners;

import de.zetor.global.apis.MCTPlayerManager;
import de.zetor.global.apis.MTCCloud;
import de.zetor.global.enums.Stats_Enums;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.sql.SQLException;

import static at.paxfu.mct.managers.createItem.createItem;

public class onClick implements Listener {

    @EventHandler
    public static void onClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if(event.getItem() != null) {

            if(event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§8| §b§lRegister §8|")) {
                Bukkit.dispatchCommand(player, "register");
            }

            //Team inv
            if(event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§8| §a§lTeam §8|")) {
                Inventory inventory = Bukkit.createInventory(null, 27, "§8| §a§lTeam §8|");
                    fillInv(inventory);

                if(MTCCloud.isServerOnline("Buildserver-1")) {
                    ItemStack buildserver = createItem(Material.WOOD_SPADE, false, (short) 0, "§aBuildserver-1", false, "§a§lONLINE", "§7" + MTCCloud.getServerOnlinePlayer("Buildserver-1") + "/" + MTCCloud.getServerMaxPlayer("Buildserver-1"));
                    inventory.setItem(10, buildserver);
                }else {
                    ItemStack buildserver = createItem(Material.WOOD_SPADE, false, (short) 0, "§aBuildserver-1", false, "§c§lOFFLINE");
                    inventory.setItem(10, buildserver);
                }

                if(player.hasPermission("mtc.dev")) {
                    if (MTCCloud.isServerOnline("Devserver-1")) {
                        ItemStack devserver = createItem(Material.COMMAND, false, (short) 0, "§bDevserver-1", false, "§a§lONLINE", "§7" + MTCCloud.getServerOnlinePlayer("Devserver-1") + "/" + MTCCloud.getServerMaxPlayer("Devserver-1"));
                        inventory.setItem(11, devserver);
                    } else {
                        ItemStack devserver = createItem(Material.COMMAND, false, (short) 0, "§bDevserver-1", false, "§c§lOFFLINE");
                        inventory.setItem(11, devserver);
                    }
                }

                if(MTCCloud.isServerOnline("Testserver-1")) {
                    ItemStack testserver = createItem(Material.BARRIER, false, (short) 0, "§cTestserver-1", false, "§a§lONLINE", "§7" + MTCCloud.getServerOnlinePlayer("Testserver-1") + "/" + MTCCloud.getServerMaxPlayer("Testserver-1"));
                    inventory.setItem(13, testserver);
                }else {
                    ItemStack testserver = createItem(Material.BARRIER, false, (short) 0, "§cTestserver-1", false, "§c§lOFFLINE");
                    inventory.setItem(13, testserver);
                }

                ItemStack friends = createItem(Material.SKULL_ITEM, false, (short) 0, "§aFriends", false, "");
                inventory.setItem(26, friends);

                if(player.hasPermission("mtc.admin")) {

                    ItemStack matchservers = createItem(Material.DIAMOND, false, (short) 0, "§c§lMatchservers", false, "");
                    inventory.setItem(15, matchservers);

                }

                    player.openInventory(inventory);
            }
            //Profile Inv
            if(event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§8| §6§lProfile §8|")) {
                Inventory inventory = Bukkit.createInventory(null, 54, "§8| §6§lProfile §8|");
                fillInv(inventory);

                ItemStack weblink = createItem(Material.WOOD_BUTTON, false, (short) 0, "§6Your Stats", false, "");
                ItemStack paper1 = null;
                try {
                    paper1 = createItem(Material.PAPER, false, (short) 0, "§6All Teamwins", false, "§b§l" + MCTPlayerManager.getPlayerStats(Stats_Enums.ALL_TEAMWINS, player));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                ItemStack playerhead = createItem(Material.SKULL_ITEM, true, (short) SkullType.PLAYER.ordinal(), "§c" + player.getName(), false, "");
                SkullMeta skullMeta = (SkullMeta) playerhead.getItemMeta();
                skullMeta.setOwner(player.getName());
                playerhead.setItemMeta(skullMeta);
                ItemStack paper2 = null;
                try {
                    paper2 = createItem(Material.PAPER, false, (short) 0, "§6Points Ever Gained", false, "§b§l" + MCTPlayerManager.getPlayerStats(Stats_Enums.POINTS_EVER_GAINED, player));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                ItemStack paper3 = createItem(Material.PAPER, false, (short) 0, "§6Paper3", false, "");
                ItemStack paper4 = createItem(Material.PAPER, false, (short) 0, "§6Paper4", false, "");
                ItemStack paper5 = createItem(Material.PAPER, false, (short) 0, "§6Paper5", false, "");
                ItemStack paper6 = createItem(Material.PAPER, false, (short) 0, "§6Paper6", false, "");
                ItemStack paper7 = createItem(Material.PAPER, false, (short) 0, "§6Paper7", false, "");
                ItemStack paper8 = createItem(Material.PAPER, false, (short) 0, "§6Paper8", false, "");
                ItemStack paper9 = createItem(Material.PAPER, false, (short) 0, "§6Paper9", false, "");
                ItemStack paper10 = createItem(Material.PAPER, false, (short) 0, "§6Paper10", false, "");
                ItemStack paper11 = createItem(Material.PAPER, false, (short) 0, "§6Paper11", false, "");
                ItemStack paper12 = createItem(Material.PAPER, false, (short) 0, "§6Paper12", false, "");



                inventory.setItem(10, paper1);
                inventory.setItem(13, playerhead);
                inventory.setItem(16, paper2);
                inventory.setItem(20, paper3);
                inventory.setItem(24, paper4);
                inventory.setItem(28, paper5);
                inventory.setItem(30, paper6);
                inventory.setItem(32, paper7);
                inventory.setItem(34, paper8);
                inventory.setItem(38, paper9);
                inventory.setItem(42, paper10);
                inventory.setItem(46, paper11);
                inventory.setItem(52, paper12);



                inventory.setItem(49, weblink);
                player.openInventory(inventory);

            }

        }

        //Friends Inv
        if(event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§8| §a§lFriends §8|")) {
            Bukkit.dispatchCommand(player, "fr menu");

    }

    }
    public static void fillInv(Inventory inv){
        ItemStack glasspane = createItem(Material.STAINED_GLASS_PANE, true, (short) 7, " ", false, "");
        ItemStack lightglasspane = createItem(Material.STAINED_GLASS_PANE, true, (short) 3, " ", false, "");
        for(int slot = 0; slot < inv.getSize(); slot++){
            inv.setItem(slot, glasspane);
        }
    }
}

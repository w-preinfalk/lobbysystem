package at.paxfu.mct.listeners;

import at.paxfu.mct.messages;
import at.paxfu.mct.utils.Arrays;
import de.dytanic.cloudnet.lib.server.info.ServerInfo;
import de.zetor.global.apis.MCTMatchManger;
import de.zetor.global.apis.MTCCloud;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.SQLException;
import java.util.Collection;

public class inventoryClick implements Listener {

    @EventHandler
    public static void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();


        if(event.getInventory().getName().equalsIgnoreCase("§8| §a§lTeam §8|")) {

            if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aBuildserver-1")) {
                MTCCloud.sendPlayer(player, "Buildserver-1");

            } else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bDevserver-1")) {
                MTCCloud.sendPlayer(player, "Devserver-1");

            }else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cTestserver-1")) {
                MTCCloud.sendPlayer(player, "Testserver-1");

            }else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aFriends")) {
                Bukkit.dispatchCommand(player, "fr menu");

            } else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§c§lMatchservers")) {
                Inventory matchinv = Bukkit.createInventory(null, 27, "§8| §c§lMatchservers §8|");
                fillInv(matchinv);
                Collection<ServerInfo> servers = MTCCloud.getGroupServers("Match");
                int i = 10;
                for(ServerInfo s : servers) {
                    int matchid = 0;
                    try {
                        matchid = MCTMatchManger.getMatchServerMatchID(s.getServiceId().getServerId());
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    ItemStack server = null;
                    try {
                        server = createItem(Material.ENDER_PEARL, false, (short) 0, "§c" + s.getServiceId().getServerId(), false, "§7" + MTCCloud.getServerOnlinePlayer(s.getServiceId().getServerId()) + "/" + MTCCloud.getServerMaxPlayer(s.getServiceId().getServerId()), "§e"+ MCTMatchManger.getMatchName(matchid), "§6Price: §7"+MCTMatchManger.getMatchPrice(matchid), "§6Teamsize: §7"+MCTMatchManger.getMatchTeamsize(matchid), "§6Sponsor: §7"+MCTMatchManger.getMatchSponsorName(matchid));
                    }catch (SQLException e){
                        e.printStackTrace();
                    }
                    matchinv.setItem(i, server);
                    i++;
                }
                player.openInventory(matchinv);
            }
        }

        if(event.getInventory().getName().equalsIgnoreCase("§8| §6§lProfile §8|")) {

            if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Your Stats")) {
                player.sendMessage("§7-------------------------------------------------");
                player.sendMessage(messages.PREFIX + "Click this link to see your stats!");
                player.sendMessage(messages.PREFIX + "§astats.mctournaments.net/player.php?name=" + player.getName());
                player.sendMessage("§7-------------------------------------------------");

                player.getOpenInventory().close();
            }
        }

        if(!Arrays.build.contains(player)) {
            event.setCancelled(true);
        }

    }

    public static void fillInv(Inventory inv){
        ItemStack glasspane = createItem(Material.STAINED_GLASS_PANE, true, (short) 7, " ", false, "");
        ItemStack lightglasspane = createItem(Material.STAINED_GLASS_PANE, true, (short) 3, " ", false, "");
        for(int slot = 0; slot < inv.getSize(); slot++){
            inv.setItem(slot, glasspane);
        }
    }

    public static ItemStack createItem(final Material material, final boolean addcolor, final short colorid, final String name, final boolean enchant, final String... lore){
        final ItemStack item;
        if (addcolor){
            item = new ItemStack(material, 1, colorid);
        }else {
            item = new ItemStack(material, 1);
        }
        final ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(name);
        meta.setLore(java.util.Arrays.asList(lore));

        if (enchant){
            meta.addEnchant(Enchantment.DAMAGE_ARTHROPODS, 1, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        }

        item.setItemMeta(meta);

        return item;
    }
}

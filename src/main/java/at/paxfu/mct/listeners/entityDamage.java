package at.paxfu.mct.listeners;

import at.paxfu.mct.managers.regionEnter;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class entityDamage implements Listener {

    @EventHandler
    public static void onDamage(EntityDamageEvent event) {
        if(event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if(!regionEnter.knockbackFFA.contains(player)) {
                event.setCancelled(true);
            }
            if(event.getCause() == EntityDamageEvent.DamageCause.FALL) {
                event.setCancelled(true);
            }
        }
    }
}

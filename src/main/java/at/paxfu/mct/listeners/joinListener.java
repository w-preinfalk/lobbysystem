package at.paxfu.mct.listeners;

import at.paxfu.mct.LobbySystem;
import at.paxfu.mct.api.BanAPI;
import at.paxfu.mct.api.banreasons;
import at.paxfu.mct.managers.lobbyInventory;
import at.paxfu.mct.messages;
import at.paxfu.mct.utils.joinTickScheduler;
import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.bridge.CloudServer;
import de.zetor.global.apis.MCTPlayerManager;
import de.zetor.global.apis.MTCCloud;
import de.zetor.global.enums.Stats_Enums;
import de.zetor.team.TeamsAPI;
import fr.minuskube.netherboard.Netherboard;
import fr.minuskube.netherboard.bukkit.BPlayerBoard;
import net.luckperms.api.LuckPerms;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.sql.SQLException;

public class joinListener implements Listener {

    @EventHandler
    public static void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        //GENERAL JOIN EVENT SETTINGS
        event.setJoinMessage("");
        player.getInventory().clear();
        player.setFoodLevel(20);
        player.setHealth(20);
        player.setGameMode(GameMode.SURVIVAL);
        player.setExp(1);
        player.setLevel(1);


        try {
            if(BanAPI.ifBannedTry(player.getUniqueId())) {
                player.sendMessage(messages.PREFIX + "----------------------------------");
                player.sendMessage(messages.PREFIX + "");
                player.sendMessage(messages.PREFIX + "§7§lYou are banned from §cMCTournaments.net!");
                player.sendMessage(messages.PREFIX + "");
                if(BanAPI.getBanReason(player.getUniqueId()) != null) {
                    if (BanAPI.getBanReason(player.getUniqueId()).equals(banreasons.HACKING)) {
                        player.sendMessage(messages.PREFIX + "§7Reason: §cHACKING");
                    } else if (BanAPI.getBanReason(player.getUniqueId()).equals(banreasons.CHAT)) {
                        player.sendMessage(messages.PREFIX + "§7Reason: §cCHAT");
                    } else if (BanAPI.getBanReason(player.getUniqueId()).equals(banreasons.CROSSTEAMING)) {
                        player.sendMessage(messages.PREFIX + "§7Reason: §cCROSSTEAMING");
                    } else if (BanAPI.getBanReason(player.getUniqueId()).equals(banreasons.NAME)) {
                        player.sendMessage(messages.PREFIX + "§7Reason: §cNAME");
                    } else if (BanAPI.getBanReason(player.getUniqueId()).equals(banreasons.SKIN)) {
                        player.sendMessage(messages.PREFIX + "§7Reason: §cSKIN");
                    } else if (BanAPI.getBanReason(player.getUniqueId()).equals(banreasons.TROLLING)) {
                        player.sendMessage(messages.PREFIX + "§7Reason: §cTROLLING");
                    }
                }
                player.sendMessage(messages.PREFIX + "");
                player.sendMessage(messages.PREFIX + "Team-Member: §c" + MCTPlayerManager.getDatabasePlayerNAME(BanAPI.bannerID(player.getUniqueId())));
                player.sendMessage(messages.PREFIX + "");
                player.sendMessage(messages.PREFIX + "BanID: §c" + BanAPI.banID(player.getUniqueId()));
                player.sendMessage(messages.PREFIX + "");
                player.sendMessage(messages.PREFIX + "----------------------------------");
                if(BanAPI.getBanReason(player.getUniqueId()) != banreasons.CHAT) {
                    MTCCloud.sendPlayer(player, "Banned-1");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        lobbyInventory.setInventory(player);

        joinTickScheduler.start(player);


        //SCOREBOARD
        BPlayerBoard board = Netherboard.instance().createBoard(player, "§3MCT §7-|- §3Lobby");

        RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
        //Parameter
        String groupname = null;
        String team = null;
        String everpoints = null;

        String server = CloudAPI.getInstance().getServerId();
        int onlineplayers = CloudServer.getInstance().getCloudPlayers().size();
        try {
            everpoints = MCTPlayerManager.getPlayerStats(Stats_Enums.POINTS_EVER_GAINED, player);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        team = TeamsAPI.getPlayerTeamName(player);
        if(team.equalsIgnoreCase("ERROR")) {
            team = "NONE";
        }

        if (provider != null) {
            LuckPerms api = provider.getProvider();
            if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("administration")) {
                groupname = "§cAdmin";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("moderator")) {
                groupname = "§3Mod";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("srdeveloper")) {
                groupname = "§bSrDev";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("developer")) {
                groupname = "§bDev";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("jrdeveloper")) {
                groupname = "§bJrDev";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("srcontent")) {
                groupname = "§aSrCont";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("content")) {
                groupname = "§aCont";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("jrcontent")) {
                groupname = "§aJrCont";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("srsupporter")) {
                groupname = "§eSrSup";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("supporter")) {
                groupname = "§eSup";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("jrsupporter")) {
                groupname = "§eJrSup";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("srbuilder")) {
                groupname = "§2SrBuilder";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("builder")) {
                groupname = "§2Builder";
            } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("jrbuilder")) {
                groupname = "§2JrBuilder";
            } else {
                groupname = "§8Player";
            }
        }


        board.setAll(
                "        ",
                "§3§lRank: ",
                "§8» " + groupname,
                "   ",
                "§3§lServer: ",
                "§8» §7" + server,
                "",
                "§3§lYour Team ",
                "§8» §7" + team,
                "              ",
                "§3§lPoints: ",
                "§8» §7#POINTS#",
                "       ",
                "§6§lSponsored by"
                );

    }

    //SCOREBOARD SCHEDULER
    public static void scoreboardScheduler() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(LobbySystem.getInstance(), new Runnable() {
            @Override
            public void run() {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    BPlayerBoard board = Netherboard.instance().createBoard(player, "§3MCT §7-|- §3Lobby");

                    RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
                    //Parameter
                    String groupname = null;
                    String team = null;
                    String everpoints = null;

                    String server = CloudAPI.getInstance().getServerId();
                    int onlineplayers = CloudServer.getInstance().getCloudPlayers().size();
                    try {
                        everpoints = MCTPlayerManager.getPlayerStats(Stats_Enums.POINTS_EVER_GAINED, player);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }

                    team = TeamsAPI.getPlayerTeamName(player);
                    if(team.equalsIgnoreCase("ERROR")) {
                        team = "NONE";
                    }

                    if (provider != null) {
                        LuckPerms api = provider.getProvider();
                        if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("administration")) {
                            groupname = "§cAdmin";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("moderator")) {
                            groupname = "§3Mod";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("srdeveloper")) {
                            groupname = "§bSrDev";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("developer")) {
                            groupname = "§bDev";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("jrdeveloper")) {
                            groupname = "§bJrDev";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("srcontent")) {
                            groupname = "§aSrCont";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("content")) {
                            groupname = "§aCont";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("jrcontent")) {
                            groupname = "§aJrCont";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("srsupporter")) {
                            groupname = "§eSrSup";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("supporter")) {
                            groupname = "§eSup";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("jrsupporter")) {
                            groupname = "§eJrSup";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("srbuilder")) {
                            groupname = "§2SrBuilder";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("builder")) {
                            groupname = "§2Builder";
                        } else if (api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup().equalsIgnoreCase("jrbuilder")) {
                            groupname = "§2JrBuilder";
                        } else {
                            groupname = "§8Player";
                        }
                    }

                    board.setAll(
                            "        ",
                            "§3§lRank: ",
                            "§8» " + groupname,
                            "   ",
                            "§3§lServer: ",
                            "§8» §7" + server,
                            "",
                            "§3§lYour Team: ",
                            "§8» §7" + team,
                            "              ",
                            "§3§lPoints: ",
                            "§8» §7#POINTS#",
                            "       ",
                            "§6§lSponsored by"
                    );

                }
            }
        }, 0L, 20L); //0 Tick initial delay, 20 Tick (1 Second) between repeats
    }
}

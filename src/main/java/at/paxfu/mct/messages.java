package at.paxfu.mct;

public interface messages {

    //GENERAL
    String PREFIX = "§8[§6MCT§8] §7";
    String NO_PERM = PREFIX + "§cYou have no permission to do this!";

    //ERRORS

    //COMMANDS
    String SETSPAWN_SUCC_MSG = PREFIX + "The spawn was set!";
    String SETSPAWN_WRONG_ALIAS = PREFIX + "Wrong usage! Use /setspawn <SPAWN/MINIGAME>";

    //MINIGAMES
    String MINIGAME_KNOCKBACKFFA_JOIN = PREFIX + "You entered the §aKnockbackFFA §7Lobby-Minigame";
    String MINIGAME_KNOCKBACKFFA_LEAVE = PREFIX + "You left the §aKnockbackFFA §7Lobby-Minigame";
}

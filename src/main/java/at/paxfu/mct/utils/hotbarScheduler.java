package at.paxfu.mct.utils;

import at.paxfu.mct.LobbySystem;
import at.paxfu.mct.managers.lobbyInventory;
import at.paxfu.mct.managers.regionEnter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class hotbarScheduler {

    public static void start(){
        Bukkit.getScheduler().scheduleSyncRepeatingTask(LobbySystem.getInstance(), new Runnable() {
            @Override
            public void run() {
                for(Player all : Bukkit.getOnlinePlayers()) {
                    if(!regionEnter.knockbackFFA.contains(all)) {
                        lobbyInventory.setInventory(all);
                    }else if(Arrays.build.contains(all)) {
                        lobbyInventory.setInventory(all);
                    }
                }
            }
        }, 0L, 100L);
    }
}

package at.paxfu.mct.utils;

import at.paxfu.mct.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class joinTickScheduler {

    public static void start(Player player) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(LobbySystem.getInstance(), new Runnable() {
            @Override
            public void run() {

                double x = LobbySystem.locations.getDouble("Spawn.X");
                double y = LobbySystem.locations.getDouble("Spawn.Y");
                double z = LobbySystem.locations.getDouble("Spawn.Z");
                float yaw = (float) LobbySystem.locations.getDouble("Spawn.YAW");
                float pitch = (float) LobbySystem.locations.getDouble("Spawn.PITCH");
                World world = Bukkit.getWorld(LobbySystem.locations.getString("Spawn.WORLD"));

                Location location = new Location(world, x, y, z, yaw, pitch);
                player.teleport(location);

            }
        }, 1L);
    }
}

package at.paxfu.mct.managers;

import at.paxfu.mct.messages;
import com.mewin.WGRegionEvents.events.RegionEnterEvent;
import com.mewin.WGRegionEvents.events.RegionLeaveEvent;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

import static at.paxfu.mct.managers.createItem.createItem;

public class regionEnter implements Listener, messages {

    public static ArrayList<Player> knockbackFFA = new ArrayList<>();

    @EventHandler
    public void onRegionEnter(RegionEnterEvent e) {
        Player p = e.getPlayer();
        if (e.getRegion().getId().equals("kkk")){
            knockbackFFA.add(p);
            p.getInventory().clear();
            p.sendMessage(messages.MINIGAME_KNOCKBACKFFA_JOIN);
            ItemStack stack = createItem(Material.STICK, false, (short) 0, "§c§lStick", false, "");
            ItemMeta meta = stack.getItemMeta();
            meta.addEnchant(Enchantment.KNOCKBACK, 2, true);
            stack.setItemMeta(meta);
            p.getInventory().setItem(0, stack);
        }
    }

    @EventHandler
    public void onRegionLeave(RegionLeaveEvent e) {
        Player p = e.getPlayer();
        if (e.getRegion().getId().equals("kkk")){
            knockbackFFA.remove(p);
            lobbyInventory.setInventory(p);
            p.setFoodLevel(20);
            p.setHealth(20);
            p.sendMessage(messages.MINIGAME_KNOCKBACKFFA_LEAVE);
        }
    }
}

package at.paxfu.mct.commands;

import at.paxfu.mct.LobbySystem;
import at.paxfu.mct.messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;

public class setspawnCMD implements CommandExecutor, messages {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if(player.hasPermission("mct.setspawn")) {
            if(args.length == 1) {
                if(args[0].equalsIgnoreCase("spawn")) {
                    LobbySystem.locations.set("Spawn.X", player.getLocation().getX());
                    LobbySystem.locations.set("Spawn.Y", player.getLocation().getY());
                    LobbySystem.locations.set("Spawn.Z", player.getLocation().getZ());
                    LobbySystem.locations.set("Spawn.PITCH", player.getLocation().getPitch());
                    LobbySystem.locations.set("Spawn.YAW", player.getLocation().getYaw());
                    LobbySystem.locations.set("Spawn.WORLD", player.getLocation().getWorld().getName());

                    try {
                        LobbySystem.locations.save(LobbySystem.locationsf);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    player.sendMessage(messages.SETSPAWN_SUCC_MSG);
                }else if(args[0].equalsIgnoreCase("minigame")) {
                    LobbySystem.locations.set("Kffa.X", player.getLocation().getX());
                    LobbySystem.locations.set("Kffa.Y", player.getLocation().getY());
                    LobbySystem.locations.set("Kffa.Z", player.getLocation().getZ());
                    LobbySystem.locations.set("Kffa.PITCH", player.getLocation().getPitch());
                    LobbySystem.locations.set("Kffa.YAW", player.getLocation().getYaw());
                    LobbySystem.locations.set("Kffa.WORLD", player.getLocation().getWorld().getName());

                    try {
                        LobbySystem.locations.save(LobbySystem.locationsf);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    player.sendMessage(messages.SETSPAWN_SUCC_MSG);
                }else {
                    player.sendMessage(messages.SETSPAWN_WRONG_ALIAS);
                }
            }else {
                player.sendMessage(messages.SETSPAWN_WRONG_ALIAS);
            }
        }else {
            player.sendMessage(messages.NO_PERM);
        }
        return false;
    }
}

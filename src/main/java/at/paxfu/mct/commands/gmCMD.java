package at.paxfu.mct.commands;

import at.paxfu.mct.messages;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class gmCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player player = (Player) sender;
        if(args.length == 1) {
            if(player.hasPermission("mct.lobby.gm")) {
                if(args[0].equalsIgnoreCase("0")) {
                    player.setGameMode(GameMode.SURVIVAL);
                    player.sendMessage(messages.PREFIX + "Dein Spielmodus wurde auf §eSURVIVAL §7geändert!");
                }else if(args[0].equalsIgnoreCase("1")) {
                    player.setGameMode(GameMode.CREATIVE);
                    player.sendMessage(messages.PREFIX + "Dein Spielmodus wurde auf §eCREATIVE §7geändert!");
                }else if(args[0].equalsIgnoreCase("2")) {
                    player.setGameMode(GameMode.ADVENTURE);
                    player.sendMessage(messages.PREFIX + "Dein Spielmodus wurde auf §eADVENTURE §7geändert!");
                }else if(args[0].equalsIgnoreCase("3")) {
                    player.setGameMode(GameMode.SPECTATOR);
                    player.sendMessage(messages.PREFIX + "Dein Spielmodus wurde auf §eSPECTATOR §7geändert!");
                }
            }else {
                player.sendMessage(messages.NO_PERM);
            }
        }else if(args.length == 2) {
            if(player.hasPermission("mct.lobby.gm.other")) {
                Player target = Bukkit.getPlayer(args[1]);
                if(args[0].equalsIgnoreCase("0")) {
                    target.setGameMode(GameMode.SURVIVAL);
                    target.sendMessage(messages.PREFIX + "Dein Spielmodus wurde auf §eSURVIVAL §7geändert!");
                    player.sendMessage(messages.PREFIX + "Der Spielmodus von §e" + target.getName() + " §7wurde auf §eSURVIVAL §7gesetzt!");
                }else if(args[0].equalsIgnoreCase("1")) {
                    target.setGameMode(GameMode.CREATIVE);
                    target.sendMessage(messages.PREFIX + "Dein Spielmodus wurde auf §eCREATIVE §7geändert!");
                    player.sendMessage(messages.PREFIX + "Der Spielmodus von §e" + target.getName() + " §7wurde auf §eCREATIVE §7gesetzt!");
                }else if(args[0].equalsIgnoreCase("2")) {
                    target.setGameMode(GameMode.ADVENTURE);
                    target.sendMessage(messages.PREFIX + "Dein Spielmodus wurde auf §eADVENTURE §7geändert!");
                    player.sendMessage(messages.PREFIX + "Der Spielmodus von §e" + target.getName() + " §7wurde auf §eADVENTURE §7gesetzt!");
                }else if(args[0].equalsIgnoreCase("3")) {
                    target.setGameMode(GameMode.SPECTATOR);
                    target.sendMessage(messages.PREFIX + "Dein Spielmodus wurde auf §eSPECTATOR §7geändert!");
                    player.sendMessage(messages.PREFIX + "Der Spielmodus von §e" + target.getName() + " §7wurde auf §eSPECTATOR §7gesetzt!");
                }
            }else {
                player.sendMessage(messages.NO_PERM);
            }
        }
        return false;
    }
}

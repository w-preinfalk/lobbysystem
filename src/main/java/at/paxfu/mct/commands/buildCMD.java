package at.paxfu.mct.commands;

import at.paxfu.mct.managers.lobbyInventory;
import at.paxfu.mct.messages;
import at.paxfu.mct.utils.Arrays;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class buildCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(sender instanceof Player) {
            Player player = (Player) sender;

            if(args.length == 0) {
                if(player.hasPermission("mct.lobby.build")) {

                    if(Arrays.build.contains(player)) {
                        //Build beenden
                        Arrays.build.remove(player);
                        player.sendMessage(messages.PREFIX + "You left the §aBuild-Mode§7!");
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 0.1F, 0.1F);
                        player.setGameMode(GameMode.SURVIVAL);
                        player.getInventory().clear();
                        lobbyInventory.setInventory(player);


                    }else {
                        //Build betreten
                        Arrays.build.add(player);
                        player.sendMessage(messages.PREFIX + "You joined the §aBuild-Mode§7!");
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 0.1F, 0.1F);
                        player.getInventory().clear();
                        player.setGameMode(GameMode.CREATIVE);
                    }
                }
            }
        }
        return false;
    }
}
